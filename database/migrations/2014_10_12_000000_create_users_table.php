<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('facebook_token');
            $table->string('google_token');
            $table->string('verify_code');
            $table->string('address');
            $table->boolean('user_status')->default('0');
            $table->boolean('status')->default('1');
            $table->string('jwt_token');
            $table->string('image')->default('default.png');
            $table->string('firebase_token');
            $table->string('lat')->default('0');
            $table->string('lng')->default('0');
            $table->string('user_type');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
