<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('subcategory_id')->unsigned();
            $table->bigInteger('user_id')->unsigned();
            $table->string('quantity');
            $table->string('total');
            $table->timestamps();
            $table->foreign('subcategory_id')
                ->references('id')->on('subcategories')
                ->onDelete('cascade')->onUpdate('cascade');


            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade')->onUpdate('cascade');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
