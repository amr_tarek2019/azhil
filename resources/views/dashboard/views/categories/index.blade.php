@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Categories DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Categories</li>
                                <li class="breadcrumb-item active">Categories DataTable</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('category.create') }}" class="btn btn-primary">Add New</a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Categories Details</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>title english</th>
                                        <th>title arabic</th>
                                        <th>logo</th>
                                        <th>status</th>
                                        <th width="10%">actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $key => $category)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$category->title_en}}</td>
                                            <td>{{$category->title_ar}}</td>
                                            <td>
                                                <img class="img-responsive img-thumbnail" src="{{ asset($category->logo) }}" style="height: 100px; width: 100px" alt="">
                                            </td>
                                            <td>
                                                    <label class="switch">
                                                        <input onchange="update_featured(this)" value="{{ $category->id }}" type="checkbox"
                                                        <?php if($category->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                            </td>
                                            <td>
                                                <a href="{{ route('category.edit',$category->id) }}" class="btn btn-info btn-sm"><i class="material-icons">edit</i></a>

                                                <form id="delete-form-{{ $category->id }}" action="{{ route('category.destroy',$category->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $category->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">delete</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('myjsfile')
    <script>
    function update_featured(el){
    if(el.checked){
    var status = 1;
    }
    else{
    var status = 0;
    }
    $.post('{{ route('category.status',$category->id) }}', {_token:'{{ csrf_token() }}', id:el.value, status:status}, function(data){
    if(data == 1){
    alert('success , category status updated successfully');
    }
    else{
    alert('danger , Something went wrong');
    }
    });
    }
    </script>
    @endsection