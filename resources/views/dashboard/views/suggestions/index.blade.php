@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Suggestions DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Suggestions</li>
                                <li class="breadcrumb-item active">Suggestions Table</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Suggestions Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>email</th>
                                        <th>suggestion</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($suggestions as $key=>$suggestion)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$suggestion->user->name}}</td>
                                            <td>{{$suggestion->user->email}}</td>
                                            <td>{{$suggestion->text}}</td>
                                            <td>{{$suggestion->created_at}}</td>
                                            <td>
                                                <a href="{{ route('suggestion.show',$suggestion->id) }}" class="btn btn-info">Details</a>

                                                <form id="delete-form-{{ $suggestion->id }}" action="{{ route('suggestion.destroy',$suggestion->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $suggestion->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>





@endsection