@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Edit Settings</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item active">Edit Settings</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">
                    <div class="col-lg-12">
                        @include('dashboard.partials.msg')
                        <form class="card" action="{{route('settings.update')}}" method="POST"  enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">Settings</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">About In English</label>
                                            <textarea class="form-control"
                                                      name="text_en" id="text_en"
                                                      rows="5" placeholder="Enter About your description">{{$settings->text_en}}</textarea>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br><br>

                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">About In Arabic</label>
                                            <textarea class="form-control" rows="5"
                                                      name="text_ar" id="text_ar"
                                                      placeholder="Enter About your description">{{$settings->text_ar}}</textarea>
                                        </div>
                                    </div>
                                    <br><br><br><br><br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">Email</label>
                                            <input class="form-control" type="email" name="email" id="email"
                                                      placeholder="Enter email"
                                                    value="{{$settings->email}}"/>
                                        </div>
                                    </div>
                                    <br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">Phone</label>
                                            <input class="form-control" type="tel" name="phone" id="phone"
                                                   placeholder="Enter phone"
                                                   value="{{$settings->phone}}"/>
                                        </div>
                                    </div>
                                    <br><br><br><br><br>
                                    <div class="col-md-12">
                                        <div class="form-group mb-0">
                                            <label class="form-label">app name</label>
                                            <input class="form-control" type="text" name="app_name" id="app_name"
                                                   placeholder="Enter phone"
                                                   value="{{$settings->app_name}}"/>
                                        </div>
                                    </div>
                                    <br><br><br><br><br>
                                    <label class="form-label">Logo</label>
                                    <img class="img-fluid rounded" src="{{ asset($settings->logo) }}" itemprop="thumbnail" alt="gallery" data-original-title="" title=""
                                         style="margin-left: 15px;max-width: 100px;">

                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Upload File</label>
                                            <div class="col-sm-12">
                                                <input class="form-control" type="file" data-original-title="" title="" id="logo" name="logo" >
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">Update settings</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection