@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Request Details</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html" data-original-title="" title=""><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
                                <li class="breadcrumb-item">Requests</li>
                                <li class="breadcrumb-item active">Request Details</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Request Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>Address</th>
                                        <th>city</th>
                                        <th>appointment</th>
                                        <th>payment</th>
                                        <th>status</th>
                                        <th>order number</th>
                                        <th>adittional info</th>
                                        <th>created at</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>{{$order->address}}</td>
                                    <td>{{$order->city->name_ar}}</td>
                                    <th>
                                        @if($order->appointment == true)
                                            <span class="label label-info">appointment</span>
                                        @else
                                            <span class="label label-danger">not appointment</span>
                                        @endif

                                    </th>
                                    <th>
                                        @if($order->appointment == true)
                                            <span class="label label-info">cash</span>
                                        @else
                                            <span class="label label-danger">paypal</span>
                                        @endif

                                    </th>

                                    <th>
                                        @if($order->status == null)
                                            <span class="label label-info">not viewed</span>
                                        @elseif($order->status == 1)
                                            <span class="label label-success">not accepted</span>
                                        @else
                                            <span class="label label-danger">accepted</span>
                                        @endif

                                    </th>
                                    <td>{{$order->order_number}}</td>
                                    <td>{{$order->additional_info}}</td>
                                    <td>{{$order->created_at}}</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>User Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>

                                        <th>name</th>
                                        <th>email</th>
                                        <th>image</th>
                                        <th>phone</th>
                                    </tr>
                                    </thead>
                                    <tbody>


                                    <td> <a href="{{route('user.show',$order->request->user->id)}}" data-original-title="" title="">{{ $order->request->user->name}}</a></td>
                                    <td>{{ $order->request->user->email}}</td>
                                    <td><img class="img-fluid img-60" src="{{ $order->request->user->image}}" alt="#" data-original-title="" title=""></td>
                                    <td>{{ $order->request->user->phone}}</td>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h5>Order Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="order-history table-responsive wishlist">
                                <table class="table table-bordernone">
                                    <thead>
                                    <tr>
                                        <th>subcategory</th>
                                        <th>quantity</th>
                                        <th>total</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <td>{{ $order->request->subcategory->name_en}}
                                    <br><br>
                                        {{ $order->request->subcategory->name_ar}}
                                    </td>
                                    <td> {{ $order->request->quantity}}</td>
                                    <td> {{ $order->request->total}}</td>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection