@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Tutorials DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Tutorials</li>
                                <li class="breadcrumb-item active">Tutorials Table</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('sliders.create') }}" class="btn btn-primary">Add New</a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Tutorials Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>title en</th>
                                        <th>title ar</th>
                                        <th>image</th>
                                        <th>status</th>
                                        <th>app type</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sliders as $key=>$slider)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$slider->title_en}}</td>
                                            <td>{{$slider->title_ar}}</td>
                                            <td><img class="img-responsive img-thumbnail" src="{{ asset($slider->image) }}" style="height: 100px; width: 100px" alt=""></td>
                                            <td>
                                                <div class="media-body text-right icon-state">
                                                    <label class="switch">
                                                        <input onchange="updateSliderStatus(this)" value="{{ $slider->id }}" type="checkbox"
                                                        <?php if($slider->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                @if($slider->app_type == true)
                                                    <span class="label label-info">technician</span>
                                                @else
                                                    <span class="label label-danger">user</span>
                                                @endif
                                            </td>
                                            <td>{{$slider->created_at}}</td>
                                            <td>
                                                <a href="{{ route('sliders.edit',$slider->id) }}" class="btn btn-info">edit</a>

                                                <form id="delete-form-{{ $slider->id }}" action="{{ route('sliders.destroy',$slider->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $slider->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
@section('myjsfile')
    <script>
        function updateSliderStatus(elUser){
            if(elUser.checked){
                var status = 1;
            }
            else{
                var status = 0;
            }
            $.post('{{ route('sliders.status',isset($slider) ? $slider->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, status:status}, function(data){
                if(data == 1){
                    alert('success , slider status updated successfully');
                }
                else{
                    alert('danger , Something went wrong');
                }
            });
        }
    </script>
@endsection