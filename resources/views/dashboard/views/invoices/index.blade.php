<!DOCTYPE html>
<html lang="en">

<body>

<div class="page-wrapper">

    <!-- Page Body Start-->
    <div class="page-body-wrapper">

        <div class="page-body">

            <!-- Container-fluid starts-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="invoice">
                                    <div>
                                        <div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="media">
                                                        <div class="media-left"><img class="media-object img-60" width="100px" src="{{ $settings->logo}}" alt=""></div>
                                                        <div class="media-body m-l-20">
                                                            <h4 class="media-heading">{{$settings->app_name}}</h4>
                                                            <p>{{$settings->email}}<br><span class="digits">{{$settings->phone}}</span></p>
                                                        </div>
                                                    </div>
                                                    <!-- End Info-->
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="text-md-right">
                                                        <h3>Invoice #<span class="digits counter">{{$data->order_number}}</span></h3>
                                                        <p>Issued:<span class="digits">{{$data->technician->user->name}}</span><br>
                                                            Payment type:@if($data->order->payment == true)
                                                            <h4>cash</h4>
                                                            @else
                                                            <h4>paypal</h4>
                                                        @endif
                                                            </p>
                                                    </div>
                                                    <!-- End Title-->
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <!-- End InvoiceTop-->
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="media">
                                                    <div class="media-left"><img class="media-object rounded-circle img-60" width="100px" src="{{$data->user->image}}" alt=""></div>
                                                    <div class="media-body m-l-20">
                                                        <h4 class="media-heading">{{$data->user->name}}</h4>
                                                        <p>{{$data->user->email}}<br><span class="digits">{{$data->user->phone}}</span></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- End Invoice Mid-->
                                        <div>
                                            <div class="table-responsive invoice-table" id="table">
                                                <table class="table table-bordered table-striped">
                                                    <tbody>
                                                    <tr>
                                                        <td class="Hours">
                                                            <h6 class="p-2 mb-0">Subcategory</h6>
                                                        </td>
                                                        <td class="Rate">
                                                            <h6 class="p-2 mb-0">Quantity</h6>
                                                        </td>
                                                        <td class="subtotal">
                                                            <h6 class="p-2 mb-0">total</h6>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <p class="m-0">{{$data->order->request->subcategory->name_en}}
                                                            <br>
                                                                {{$data->order->request->subcategory->name_ar}}
                                                            </p>
                                                        </td>
                                                        <td>
                                                            <p class="itemtext digits">{{$data->order->request['quantity']}}</p>
                                                        </td>
                                                        <td>
                                                            <p class="itemtext digits">{{$data->order->request['total']}}</p>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                            <!-- End Table-->

                                        </div>
                                        <!-- End InvoiceBot-->
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Container-fluid Ends-->
        </div>

    </div>
</div>

</body>
</html>