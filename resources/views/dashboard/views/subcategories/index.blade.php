@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Subcategories DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Dashboard</li>
                                <li class="breadcrumb-item">Subcategories</li>
                                <li class="breadcrumb-item active">Subcategories DataTable</li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <a href="{{ route('subcategory.create') }}" class="btn btn-primary">Add New</a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Subcategories Details</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>name english</th>
                                        <th>name arabic</th>
                                        <th>image</th>
                                        <th>status</th>
                                        <th width="10%">actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subcategories as $key => $subcategory)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$subcategory->name_en}}</td>
                                            <td>{{$subcategory->name_ar}}</td>
                                            <td>
                                                <img class="img-responsive img-thumbnail" src="{{ asset($subcategory->image) }}" style="height: 100px; width: 100px" alt="">
                                            </td>
                                            <td>
                                                    <label class="switch">
                                                        <input onchange="updateSubcategoryStatus(this)" value="{{ $subcategory->id }}" type="checkbox"
                                                        <?php if($subcategory->status == 1) echo "checked";?>>
                                                        <span class="switch-state bg-primary"></span>
                                                    </label>
                                            </td>
                                            <td>
                                                <a href="{{ route('subcategory.edit',$subcategory->id) }}" class="btn btn-info btn-sm"><i class="material-icons">edit</i></a>

                                                <form id="delete-form-{{ $subcategory->id }}" action="{{ route('subcategory.destroy',$subcategory->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $subcategory->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }"><i class="material-icons">delete</i></button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('myjsfile')
    <script>
    function updateSubcategoryStatus(ell){
    if(ell.checked){
    var status = 1;
    }
    else{
    var status = 0;
    }
    $.post('{{ route('subcategory.status',$subcategory->id) }}', {_token:'{{ csrf_token() }}', id:ell.value, status:status}, function(data){
    if(data == 1){
    alert('success , subcategory status updated successfully');
    }
    else{
    alert('danger , Something went wrong');
    }
    });
    }
    </script>
    @endsection