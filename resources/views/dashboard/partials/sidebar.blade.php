<div class="page-sidebar">

    <div class="sidebar custom-scrollbar">
        <div class="sidebar-user text-center">
            <div><img class="img-60 rounded-circle" src="{{Auth::User()->image}}" alt="#">
                <div class="profile-edit"><a href="{{route('profile')}}" target="_blank"><i data-feather="edit"></i></a></div>
            </div>
            <h6 class="mt-3 f-14">{{Auth::user()->name}}</h6>
            <p>{{Auth::user()->email}}</p>
        </div>
        <ul class="sidebar-menu">
            <li><a class="sidebar-header" href="{{route('dashboard')}}"><i data-feather="home"></i><span>Dashboard</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('user.index')}}"><i data-feather="users"></i><span>Users</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('admin.index')}}"><i data-feather="users"></i><span>Admins</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('member.index')}}"><i data-feather="users"></i><span>Members</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('technician.index')}}"><i data-feather="users"></i><span>Technicians</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('category.index')}}"><i data-feather="layers"></i><span>Categories</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('subcategory.index')}}"><i data-feather="list"></i><span>Subcategories</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('orders.index')}}"><i data-feather="tag"></i><span>Orders</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('reservations.index')}}"><i data-feather="clipboard"></i><span>Reservations</span></a>
            </li>

            <li><a class="sidebar-header"><i data-feather="file-text"></i><span>Reports</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="general-widget.html"><i class="fa fa-circle"></i>Users</a></li>
                    <li><a href="chart-widget.html"><i class="fa fa-circle"></i>Technicians</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header"><i data-feather="star"></i><span>Ratings</span><i class="fa fa-angle-right pull-right"></i></a>
                <ul class="sidebar-submenu">
                    <li><a href="general-widget.html"><i class="fa fa-circle"></i>Users</a></li>
                    <li><a href="chart-widget.html"><i class="fa fa-circle"></i>Technicians</a></li>
                </ul>
            </li>

            <li><a class="sidebar-header" href="#"><i data-feather="server"></i><span>Scheduling</span></a>
            </li>
            <li><a class="sidebar-header" href="#"><i data-feather="bell"></i><span>Notifications</span></a>
            </li>

            <li><a class="sidebar-header" href="{{route('sliders.index')}}"><i data-feather="image"></i><span>Tutorial Sliders</span></a>
            </li>

            <li><a class="sidebar-header" href="{{route('suggestion.index')}}"><i data-feather="mail"></i><span>Suggestions</span></a>
            </li>
            <li><a class="sidebar-header" href="{{route('settings')}}"><i data-feather="settings"></i><span>Settings</span></a>
            </li>
        </ul>
    </div>
</div>
