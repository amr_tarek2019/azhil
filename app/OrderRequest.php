<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OrderRequest extends Model
{
    protected $table='order_request';
    protected $fillable=[ 'user_id', 'technician_id', 'order_id', 'order_number', 'status', 'order_type'];

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function technician()
    {
        return $this->belongsTo('App\Technician');
    }

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function subcategory()
    {
        return $this->belongsTo('App\Subcategory');
    }

    public function request()
    {
        return $this->belongsTo('App\Request');
    }
}
