<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicianRate extends Model
{
    protected $table='technicians_rate';
    protected $fillable=['user_id', 'technician_id', 'order_id', 'rate', 'feedback'];


    public function technician()
    {
        return $this->belongsTo('App\Technician');
    }
}
