<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $table='contacts';
    protected $fillable=['text'];
    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
