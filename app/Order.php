<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='orders';
    protected $fillable=['request_id', 'address', 'city_id', 'lat', 'lng', 
        'appartment', 'floor', 'total_price', 'additional_info', 'appointment', 'payment', 
        'status', 'month', 'day', 'time'];

    public function request()
    {
        return $this->belongsTo('App\Request');
    }

    public function city()
    {
        return $this->belongsTo('App\City');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->toDateString();
    }
}
