<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceProgress extends Model
{
    protected $table='services_progress';
    protected $fillable=['technician_id', 'order_id', 'service_cost'];
}
