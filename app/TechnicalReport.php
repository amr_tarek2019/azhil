<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicalReport extends Model
{
    protected $table='technical_reports';
    protected $fillable=['category_id','components','note'];
}
