<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table='settings';
    protected $fillable=['text_en','text_ar','logo','email','phone','app_name'];

    public function getLogoAttribute($value)
    {
        if ($value) {
            return asset('uploads/settings/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }

    public function setLogoAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/settings/'),$imageName);
            $this->attributes['logo']=$imageName;
        }
    }
}
