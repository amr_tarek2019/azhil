<?php

namespace App\Http\Controllers\Dashboard;

use App\Tutorial;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SlidersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = Tutorial::all();
        return view('dashboard.views.tutorials.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.views.tutorials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title_ar' => 'required',
            'title_en' => 'required',
            'text_en' => 'required',
            'text_ar' => 'required',
            'image' => 'required|mimes:jpeg,jpg,bmp,png',
            'app_type'=>'required',
            'status'=>'required',
        ]);
        $slider = new Tutorial();
        $slider->title_ar = $request->title_ar;
        $slider->title_en = $request->title_en;
        $slider->text_en = $request->text_en;
        $slider->text_ar = $request->text_ar;
        $slider->image =  $request->image;
        $slider->app_type=$request->app_type;
        $slider->status=$request->status;
        if ($slider->save())
        {
            return redirect()->route('sliders.index')->with('successMsg','Slider Created Successfully');
        }
        return redirect()->route('sliders.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slider = Tutorial::find($id);
        return view('dashboard.views.tutorials.edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider=Tutorial::find($request->id);
        $slider->title_ar = $request->title_ar;
        $slider->title_en = $request->title_en;
        $slider->text_en = $request->text_en;
        $slider->text_ar = $request->text_ar;
        $slider->image =  $request->image;
        $slider->app_type=$request->app_type;
        if ($slider->save())
        {
            return redirect()->route('sliders.index')->with('successMsg','Slider Updated Successfully');
        }
        return redirect()->route('sliders.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider = Tutorial::find($id);
        $slider->delete();
        return redirect()->back()->with('successMsg','Slider Successfully Delete');
    }


    public function updateStatus(Request $request)
    {
        $slider = Tutorial::findOrFail($request->id);
        $slider->status = $request->status;
        if($slider->save()){
            return 1;
        }
        return 0;
    }
}
