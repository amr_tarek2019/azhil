<?php

namespace App\Http\Controllers\Dashboard;

use App\OrderRequest;
use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reservations=OrderRequest::all();
        return view('dashboard.views.reservations.index',compact('reservations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reservation = OrderRequest::find($id);
        return view('dashboard.views.reservations.show',compact('reservation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        OrderRequest::find($id)->delete();
        return redirect()->back()->with('successMsg','Reservation Successfully Delete');
    }


    function generatePdf($id) {
        $settings=Settings::first();
        $data = OrderRequest::find($id);
        $pdf = \niklasravnsborg\LaravelPdf\Facades\Pdf::loadView('dashboard.views.invoices.index', compact('data','settings'));
        return $pdf->download('reservation.pdf');
    }
}
