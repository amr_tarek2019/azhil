<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Technician;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TechniciansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.views.technicians.index')->with('users',User::where('user_type','technician')->get());

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('dashboard.views.technicians.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'password'=>'required',
            'job'=>'required',
            'category' => 'required',
            'phone' => 'required',
        ]);
        $user=User::where('email',$request->email)->orWhere('phone',$request->phone)->exists();
        if ($user){
            return redirect()->route('user.index')->with('successMsg','User Created Before');
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password,
            'phone'=>$request->phone,
            'user_type'=>'technician',
            'user_status'=>'0',
            'status'=>'1',
            'verify_code'=>'0',
            'jwt_token'=>'0',
            'latitude'=>'0',
            'longitude'=>'0',
            'facebook_token'=>'0',
            'google_token'=>'0',
            'address'=>'0',
            'firebase_token'=>'0'
        ]);
        $technician=New Technician();
        $technician->user_id=$user->id;
        $technician->job=$request->job;
        $technician->phone=$request->phone;
        $technician->category_id = $request->category;
        $technician->status='0';
        if ( $technician->save())
        {
            return redirect()->route('technician.index')->with('successMsg','technician Successfully Created');
        }
        return redirect()->route('technician.create')->with('successMsg','sorry something went wrong');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::all();
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();
        return view('dashboard.views.technicians.edit',compact('user','technician','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $technician=Technician::where('user_id',$id)->first();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password =$request->password;
        $technician->job=$request->job;
        $technician->status=$request->status;
        $technician->category_id=$request->category;

        $user->save();
        $technician->save();
            return redirect()->route('technician.index')->with('successMsg','User Successfully Updated');


        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if($user)
        {
            return redirect()->route('technician.index')->with('successMsg','User Created Before');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('successMsg','technician Successfully Delete');
    }

    public function updateStatus(Request $request)
    {
        $user = User::findOrFail($request->id);
        $user->user_status = $request->user_status;
        if($user->save()){
            return 1;
        }
        return 0;
    }

    public function technicianAvailability(Request $request)
    {
        $technician = Technician::findOrFail($request->id);
        $technician->status = $request->status;
        if($technician->save()){
            return 1;
        }
        return 0;
    }
}
