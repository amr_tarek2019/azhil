<?php

namespace App\Http\Controllers\Api\technician;

use App\Category;
use App\ReportOrder;
use App\TechnicalReport;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ReportOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
       $technicalReportComponent = TechnicalReport::where('id', $request->id)->select('components')->first();
       $technicalReportCategory = Category::where('id', $request->id)->select('title_'.$lang.' as title')->first();

       $data['category']=$technicalReportCategory;
        $data['component']=$technicalReportComponent;
        $response=[
            'message'=>'get data of user report successfully',
            'status'=>200,
            'data'=>$data,
        ];
        return \Response::json($response,200);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->select('id')->first();
//        $user = User::where('jwt_token',$jwt)->first();
//        TechnicalReport::where('id',$request->id)->select('id')->first();
        $validator = Validator::make($request->all(), [
            'price' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
       $report=new ReportOrder();
        $report->technician_id=$user->id;
       $report->technical_report_id=$request->report_id;
       $report->price=$request->price;

        $report->save();
        $response=[
            'message'=>'waiting the approve for your cost',
            'status'=>200,
        ];
//        return $response;
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
