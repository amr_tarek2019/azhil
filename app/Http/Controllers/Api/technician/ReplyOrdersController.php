<?php

namespace App\Http\Controllers\Api\technician;

use App\City;
use App\Order;
use App\OrderRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ReplyOrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        $orders= Order::where('id',$request->id)->Where('status','null')
//            ->select('id','request_id','address','city_id',
//                'appartment','floor','total_price','additional_info','appointment','payment')->get();
        //$data['order']=$order;
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $orders= Order::where('id',$request->id)->Where('status','null')->get();
        $order_item = [];
        $order_list  = [];
        foreach ($orders as $order) {
            $order_item['id'] = $order->id;
            $city = City::where('id',$order->city_id)->select('name_'.$lang.' as name')->first();
            $order_item['city'] = $city;
            $requests = \App\Request::where('id',$order->request_id)->select('quantity','total','user_id','subcategory_id')->first();
            $order_item['request'] = $requests;
            $order_item['address'] = $order->address;
            $order_item['appartment'] = $order->appartment;
            $order_item['floor'] = $order->floor;
            $order_item['total_price'] = $order->total_price;
            $order_item['additional_info'] = $order->additional_info;
            $order_list = $order_item;
        }
        $data['order'] = $order_list;
        $response = [
            'message' => 'get data of order successfully',
            'status' => 200,
            'data' => $data,
        ];
        return \Response::json($response, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order= Order::where('id',$request->id)->Where('status','null')->first();
        $validator = Validator::make($request->all(), [
            'status' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $order->status=$request->status;

        if($order->save())
        {
            $response=[
                'message'=>'order reply sent successfully',
                'status'=>200,
            ];

        }
        return \Response::json($response,200);
        if ($user){
            $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
                ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
            return $response=[
                'success'=>401,
                'message'=>'this acount submitted before',
            ];
        }

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
