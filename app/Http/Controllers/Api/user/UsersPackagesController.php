<?php

namespace App\Http\Controllers\Api\user;

use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\Package;
use App\User;
use App\UserPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersPackagesController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $categories=Category::select('id','logo','title_'.$lang. ' as title')
            ->where('status',1)->get();
        $package=Package::where('id',$request->id)->select('id','price','total')->first();

        $data['categories']=$categories;
        $data['package']=$package;

        if (!empty($data))
        {
            $response=[
                'message'=>'get data of categories successfully',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'no data found',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $packageid=Package::where('id',$request->id)->pluck('id')->first();
        $packageprice=Package::where('id',$request->id)->pluck('price')->first();
        $cat_id=Category::where('id',$request->category_id)->pluck('id')->first();


        $validator = Validator::make($request->all(), [
            'name'=>'required',
            'address'=>'required',
            'phone'=>'required',
            'email'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $userPackage=new UserPackage();
        $userPackage->user_id=$user->id;
        $userPackage->package_id=$packageid;
        $userPackage->price=$packageprice;
        $userPackage->category_id=$cat_id;
        $userPackage->name = $request->name;
        $userPackage->email = $request->email;
        $userPackage->phone = $request->phone;
        $userPackage->address = $request->address;
        $userPackage->save();
        $response=[
            'message'=>'package reserved successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();

        $userPackages = UserPackage::where('user_id',$user->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($userPackages as $res) {
            $res_item['id'] = $res->id;
            $package = \App\Package::where('id',$res->package_id)->select('id','name_'.$lang. ' as name','price')->first();
            $res_item['package_name'] = $package->name;
            $res_item['package_price'] = $package->price;
            $category = \App\Category::where('id',$res->category_id)->select('id','title_'.$lang. ' as title')->first();
            $res_item['category_name'] = $category->title;
            $res_item['name'] = $res->name;
            $res_item['subscription_id'] = $res->package_id;
            $res_item['address'] = $res->address;
            $res_item['phone'] = $res->phone;
            $res_item['email'] = $res->email;
            $res_list[] = $res_item;

        }
        //return $res_list;


        $response = [
            'message' => 'get data of history successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
        if (!$userPackages)
        {
            return \Response::json('packages not found',404);
        }else{
//            return \Response::json($wasteContainer,200);
            $response=[
                'message'=>'get data of user packages successfully',
                'status'=>200,
                'data'=>$userPackages,
            ];
            return \Response::json($response,200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token', $jwt)->first();
        $packageid = Package::where('id', $request->id)->pluck('id')->first();
        $packageprice = Package::where('id', $request->id)->pluck('price')->first();
        $cat_id = Category::where('id', $request->category_id)->pluck('id')->first();


        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $userPackage = UserPackage::where('id', $request->package_id)->select('id')->first();
        $userPackage->user_id = $user->id;
        $userPackage->package_id = $packageid;
        $userPackage->price = $packageprice;
        $userPackage->category_id = $cat_id;
        $userPackage->name = $request->name;
        $userPackage->email = $request->email;
        $userPackage->phone = $request->phone;
        $userPackage->address = $request->address;
        //return $userPackage;
        if($userPackage->save())
        {
            $response=[
                'message'=>'package data changed successfully',
                'status'=>200,
            ];
        }
            return \Response::json($response, 200);
            if (!$request->headers->has('jwt')) {
                return response(401, 'check_jwt');
            } elseif (!$request->headers->has('lang')) {
                return response(401, 'check_lang');
            }
        }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
