<?php

namespace App\Http\Controllers\Api\user;

use App\Technician;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TrackTechnicianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();

        $data['lat'] = $user['lat'];
        $data['lng'] = $user['lng'];


        $tecnician_track=User::where('id',$request->technician_id)->where('user_type','technician')->select('lat','lng','name','email','image','address')->first();

       $data['track']=$tecnician_track;
        if($data){
            $response=[
                'message'=>'get track of technician successfully',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'data not found',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
