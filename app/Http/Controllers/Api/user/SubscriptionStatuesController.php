<?php

namespace App\Http\Controllers\Api\user;

use App\OrderRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubscriptionStatuesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexVisits(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $orderRequests = OrderRequest::select('id','order_id','technician_id','order_number','order_type')
            ->where('user_id',$user->id)->Where('order_type','0')->get();
        $data['visites'] = $orderRequests;
        if($data)
        {
            $response=[
                'message'=>'vistits requests',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'Home',
                'status'=>404,
                'data'=>'no record found',
            ];
        }
        return \Response::json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexInProgress(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $orderRequests = OrderRequest::
            where('user_id',$user->id)->Where('order_type','1')->select('id','order_id','technician_id',
            'order_number','order_type')->get();
        $data['progress'] = $orderRequests;
        if($data)
        {
            $response=[
                'message'=>'progress requests',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'Home',
                'status'=>404,
                'data'=>'no record found',
            ];
        }
        return \Response::json($response,200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function indexDone(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $doneRequests = OrderRequest::where('user_id',$user->id)->Where('order_type','2')
            ->select('id','order_id','technician_id',
            'order_number','order_type')->get();
        $data['done'] = $doneRequests;
        if($data)
        {
            $response=[
                'message'=>'done requests',
                'status'=>200,
                'data'=>$data,
            ];
        }else{
            $response=[
                'message'=>'Home',
                'status'=>404,
                'data'=>'no record found',
            ];
        }
        return \Response::json($response,200);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
