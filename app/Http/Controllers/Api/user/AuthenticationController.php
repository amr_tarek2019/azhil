<?php

namespace App\Http\Controllers\Api\user;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthenticationController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address'=>'required',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
            return $response=[
                'status'=>301,
                'message'=>'this account submitted before',
            ];
        }
            $user = User::create(array_merge($request->all(),[
                'jwt_token' => $jwt_token,
                'verify_code' => $verify_code,
                'facebook_token'=>'null',
                'google_token'=>'null',
                'firebase_token'=>'null'
            ]));
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $response=[
                'status'=>200,
                'data'=>$data,
                'message'=>'User register successfully.',
            ];
            return \Response::json($response,200);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerFacebook(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address'=>'required',
            'password' => 'required',
            'facebook_token'=>'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
            return $response=[
                'status'=>301,
                'message'=>'this account submitted before',
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
            'user_status'=>'1',
            'google_token'=>'null',
            'firebase_token'=>'null'
        ]));
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['password'] = $user['password'];
        $data['jwt_token'] = $user['jwt_token'];
        $data['verify_code'] = $user['verify_code'];
        $response=[
            'status'=>200,
            'data'=>$data,
            'message'=>'User register successfully.',
        ];
        return \Response::json($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerGoogle(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'address'=>'required',
            'password' => 'required',
            'google_token'=>'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $verify_code = rand(1111, 9999);
        $jwt_token = Str::random(25);
        $user=User::where('phone',$request->phone)->orWhere('email',$request->email)->exists();
        if ($user){
            return $response=[
                'status'=>301,
                'message'=>'this account submitted before',
            ];
        }
        $user = User::create(array_merge($request->all(),[
            'jwt_token' => $jwt_token,
            'verify_code' => $verify_code,
            'user_status'=>'1',
            'facebook_token'=>'null',
            'firebase_token'=>'null'
        ]));
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['password'] = $user['password'];
        $data['jwt_token'] = $user['jwt_token'];
        $data['verify_code'] = $user['verify_code'];
        $response=[
            'status'=>200,
            'data'=>$data,
            'message'=>'User register successfully.',
        ];
        return \Response::json($response,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function verifyCode(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'verify_code' => 'required',
            'firebase_token'=>'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::where('verify_code', request('verify_code'))->first();
        if (!empty($user)) {
            $user->verify_code="null";
            $user->user_status=1;
            $user->firebase_token=$request->firebase_token;
            $user->save();
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $data['firebase_token'] = $user['firebase_token'];
            $response=[
                'message'=>'code verified successfully',
                'status'=>200,
                'data'=>$data,

            ];

            return \Response::json($response,200);

        }else{
            // return \Response::json('code activation not found',404);
            $response=[
                'message'=>'code not found',
                'status'=>404,
            ];

            return \Response::json($response,200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forgetPassword(Request $request)
    {
        $this->validate($request,[
            'phone' => 'required_if:phone,p',
            'email'=>'required_if:email,e'
        ]);
        $user = User::where('phone', request('phone'))->orWhere('email',request('email'))->first();
        $verify_code = rand(1111, 9999);

        if (!empty($user)) {
            $user->verify_code=$verify_code;
            $user->save();
            $response=[
                'message'=>'code has sent successfully',
                'status'=>200,
            ];
            return \Response::json($response,200);

        }else{
            // return \Response::json('phone not found',404);
            $response=[
                'message'=>'phone or email not found',
                'status'=>402,
            ];
            return \Response::json($response,402);
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function resetPassword(Request $request)
    {
//        $user=User::find($request->id);
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token', $jwt)->first();
        $this->validate($request,[
            'password' => 'required',
            'password_confirmation'=>'required',
        ]);
        if ($request->password !=null&&($request->password==$request->password_confirmation)){
            $user->password=$request->password_confirmation;
            if ($user->save())
            {
            $response=[
                'message'=>'password changed successfully',
                'status'=>200,
                'data'=>$user,
            ];
            }
            return \Response::json($response,200);
        }else{
            $response=[
                'message'=>'something went wrong',
                'status'=>400,
            ];

            return \Response::json($response,401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'firebase_token' => 'required',
            'phone' => 'required_if:phone,p',
            'name'=>'required_if:name,n'
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if (auth()->attempt(['phone' => $request->input('key'),
            'password' => $request->input('password')]) || auth()->attempt(['name' => $request->input('key'),
                'password' => $request->input('password')]) ) {
            $user = auth()->user();
            $verify_code = rand(1111, 9999);
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['jwt_token'] = $user['jwt_token'];
            $data['verify_code'] = $user['verify_code'];
            $data['firebase_token'] = $user['firebase_token'];
            if($user->user_status=='0')
            {
                if($user){
                    $user->verify_code=$verify_code;
                    $user->save();
                }
                return response()->json([
                    'status'=>303,
                    'message'=>'code not active',
                    'verify_code' => $verify_code,
                ]);
            }
            if($user) {
                $user->firebase_token = $request->firebase_token;
                $user->save();
                return response()->json([
                    'status'=>200,
                    'message'=>'user logged in',
                    'data'=>$data
                ]);
            }
        } else {
            return response()->json([
                'status'=>404,
                'message'=>'something wrong',
            ]);
        }
    }

    public function loginFacebook(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'facebook_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::where('facebook_token',$request->facebook_token)->first();
        if ($user){
            return response()->json([
                'status'=>200,
                'message'=>'user logged in',
                'data'=>$user
            ]);
        } else{
            return response()->json([
                'status'=>401,
                'message'=>'facebook not found',
            ]);
        }
    }

    public function loginGoogle(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'google_token' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::where('google_token',$request->google_token)->first();
        if ($user){
            return response()->json([
                'status'=>200,
                'message'=>'user logged in',
                'data'=>$user
            ]);
        } else{
            return response()->json([
                'status'=>401,
                'message'=>'google not found',
            ]);
        }
    }
}
