<?php

namespace App\Http\Controllers\Api\user;

use App\Order;
use App\OrderRequest;
use App\Subcategory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
        $getCountHistories=OrderRequest::where('user_id',$user->id)->count();
        $getCountPending=\App\Request::where('user_id',$user->id)->count();
        $histories = OrderRequest::where('user_id', $user->id)->where('status', 1)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($histories as $res) {
            $res_item['id'] = $res->id;
            $technician = \App\Technician::where('id',$res->technician_id)->select('id','job','user_id')->first();
            $res_item['job'] = $technician->job;
            $res_item['technician_name'] = \App\User::where('id',$technician->user_id)->where('user_type','technician')->pluck('name')->first();
            $res_item['order_number'] = $res->order_number;
            $res_item['created_at'] = $res->created_at;
            $order_total= Order::where('id',$res->order_id)->select('total_price')->first();
            $res_item['price'] = $order_total->total_price;
            $res_list[] = $res_item;
        }

        $userRequests = \App\Request::where('user_id', $user->id)->get();
        $request_item = [];
        $request_list  = [];
        foreach ($userRequests as $req) {
            $request_item['id'] = $req->id;
            $subcategory =  \App\Subcategory::where('id',$req->subcategory_id)->select('id','name_'.$lang.' as name')->first();
            $request_item['subcategory'] = $subcategory->name;
            $request_item['quantity'] = $req->quantity;
            $request_item['total'] = $req->total;
            $request_list[] = $request_item;
        }

        $data['countHistory'] = $getCountHistories;
        $data['countPending'] = $getCountPending;
        $data['history']=$res_list;
        $data['pending']=$request_list;
//        return $data;
        $response = [
            'message' => 'get data of User services successfully',
            'status' => 200,
            'data' => $data,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
