<?php

namespace App\Http\Controllers\Api\user;

use App\Order;
use App\PushNotification;
use Illuminate\Http\Request;
use App\Http\Controllers\Api\BaseController as BaseController;
use Illuminate\Support\Facades\Validator;

class OrderController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token',$jwt)->first();
        $token = \App\User::select('firebase_token')->where('firebase_token',$jwt)->pluck('firebase_token')->toArray();
        PushNotification::send($token,'done',1);


        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'city_id' => 'required',
            'lat'=>'required',
            'lng'=>'required',
            'appartment'=>'required',
            'floor'=>'required',
            'additional_info'=>'required',
        ]);
        if ($validator->fails()) {
            $response=[
                'message'=>'failed to booking',
                'status'=>404,
            ];
        }
        $price = \App\Request::where('id',$request->subcategory_id)->select('total')->first();
//      return $price;
        for($i=0;$i<count($request->subcategory_id);$i++)
        {
            $quantity=$request->quantity[$i];
            $total=$quantity*$price['total'];
            $requests=\App\Request::find($request->subcategory_id[$i]);
            $requests->total_price=$total;
            $order=new Order();
            $order->request_id=$request->subcategory_id[$i];
            $order->total_price=$total;
            $order->address=$request->address;
            $order->lat=$request->lat;
            $order->lng=$request->lng;
            $order->appartment=$request->appartment;
            $order->floor=$request->floor;
            $order->city_id=$request->city_id;
            $order->additional_info=$request->additional_info;
            $order->appointment=$request->appointment;
            $order->payment=$request->payment;
            $order->day=$request->day;
            $order->month=$request->month;
            $order->time	=$request->time;

            $order->save();
        }
        $response=[
            'message'=>'Reservation request sent successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
