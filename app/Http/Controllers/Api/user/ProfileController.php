<?php

namespace App\Http\Controllers\Api\user;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\BaseController as BaseController;

class ProfileController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;

        $user = User::where('jwt_token',$jwt)->first();
        $data['id'] = $user['id'];
        $data['name'] = $user['name'];
        $data['email'] = $user['email'];
        $data['phone'] = $user['phone'];
        $data['image'] = $user['image'];
        $data['address'] = $user['address'];
        $data['password'] = $user['password'];
        if ($user)
        {
        $response=[
            'message'=>'get data of user profile successfully',
            'status'=>200,
            'data'=>$data,
        ];
        }else{
            $response=[
                'message'=>'data not found',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();
         $validator = Validator::make($request->all(), [
             'name' => 'required',
             'email' => 'required',
             'address'=>'required',
             'phone'=>'required',
             'image'=>'required|mimes:jpeg,jpg,bmp,png',
             'password'=>'required',
         ]);
         if ($validator->fails()) {
             return $this->sendError('Validation Error.', $validator->errors());
         }
        $user->name = $request->name;
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->image=$request->image;
        $user->address=$request->address;
        $user->password=$request->password;
        if($user->save())
        {
            $data['id'] = $user['id'];
            $data['name'] = $user['name'];
            $data['email'] = $user['email'];
            $data['phone'] = $user['phone'];
            $data['image'] = $user['image'];
            $data['password'] = $user['password'];
            $data['address'] = $user['address'];
            $response=[
                'message'=>'profile changed successfully',
                'status'=>200,
                'data'=> $data
            ];

        }
        return \Response::json($response,200);
        if ($user){
            $user=User::where('phone',$request->phone)->orWhere('email',$request->email)
                ->orWhere('name',$request->name)->orWhere('city_id',$request->city_id)->exists();
            return $response=[
                'success'=>401,
                'message'=>'this acount submitted before',
            ];
        }

        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
