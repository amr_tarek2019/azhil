<?php

namespace App\Http\Controllers\Api\user;

use App\Category;
use App\Http\Controllers\Api\BaseController;
use App\Subcategory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SearchController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        if ($request->search)
        {
            if($lang=='en')
            {
                $validator = Validator::make($request->all(), [
                    'search' => 'required',
                ]);
                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors());
                }
                $title_en = $request->search;
                $categories = Category::select('id','title_'.$lang. ' as title')->where([
                    ['title_en', 'LIKE', '%' . $title_en . '%']])->where('status', 1)->get();
                $response=[
                    'message'=>'get data of categories successfully',
                    'status'=>200,
                    'data'=>$categories,
                ];
                return \Response::json($response,200);
            }
            else{
                $validator = Validator::make($request->all(), [
                    'search' => 'required',
                ]);
                if ($validator->fails()) {
                    return $this->sendError('Validation Error.', $validator->errors());
                }
                $title_ar = $request->search;
                $categories = Category::select('id','title_'.$lang. ' as title'
                )->where([
                    ['title_ar', 'LIKE', '%' . $title_ar . '%']])->where('status', 1)->get();
                $response=[
                    'message'=>'get data of categories successfully',
                    'status'=>200,
                    'data'=>$categories,
                ];
                return \Response::json($response,200);
            }
        }else{
            $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
            $categories=Category::select('id','logo','title_'.$lang. ' as title')
                ->where('status',1)->get();
            if (!empty($categories))
            {
                $response=[
                    'message'=>'get data of categories successfully',
                    'status'=>200,
                    'data'=>$categories,
                ];
            }else{
                $response=[
                    'message'=>'no data found',
                    'status'=>404,
                ];
            }
            return \Response::json($response,200);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filterIndex(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $categories=Category::select('id','logo','title_'.$lang. ' as title')
            ->where('status',1)->get();
        if (!empty($categories))
        {
            $response=[
                'message'=>'get data of categories successfully',
                'status'=>200,
                'data'=>$categories,
            ];
        }else{
            $response=[
                'message'=>'no data found',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getCategoryFilter(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $user = User::where('jwt_token',$jwt)->first();
        $validator = Validator::make($request->all(), [
            'longitude' => '',
            'latitude' => '',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user->longitude = $request->longitude;
        $user->latitude=$request->latitude;

        $technicians_list=Technician::select("id",'name_'.$lang. ' as name'
                ,'latitude','longitude','region_'.$lang. ' as region','rate','image','description_'.$lang. ' as description',
                'icon','image1','image2'
                ,DB::raw("3959 * acos(cos(radians(" . $user->latitude . ")) 
        * cos(radians(latitude)) 
        * cos(radians(longitude) - radians(" .  $user->longitude . ")) 
        + sin(radians(" .$user->latitude. ")) 
        * sin(radians(latitude))) AS distance"))->orderBy('distance', 'asc')->get();

        $subcategories=Subcategory::where('category_id', $request->category_id)->where('status',1)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($subcategories as $res) {
            $res_item['id'] = $res->id;
            $res_item['image'] = $res->image;
            $res_item['category'] = Category::where('id',$res->id)->select('id','logo','title_'.$lang.' as title')->first();
            $res_item['name'] = Subcategory::where('id', $res->id)->select('name_'.$lang.' as name')->first();
            $res_item['status'] = $res->status;
            $res_item['price'] = $res->price;
            $res_list[] = $res_item;
        }

        $technicianRates = Technician::whereBetween('rate',[$request->rate_from,$request->rate_to])->select('id','name_'.$lang. ' as name'
            ,'latitude','longitude','image')->get();





        $data['technician_rates'] = $technicianRates;
        $data['search_subcategories'] = $res_list;
        $data['technicians_list'] = $technicians_list;





        $response = [
            'message' => 'get data of filtering successfully',
            'status' => 200,
            'data' => $data,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
