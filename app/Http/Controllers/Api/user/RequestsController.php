<?php

namespace App\Http\Controllers\Api\user;


use App\Http\Controllers\Api\BaseController as BaseController;


use App\Subcategory;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RequestsController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        $validator = Validator::make($request->all(), [
            'quantity'=>'required',
        ]);
        if ($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
        }
        $price =Subcategory::where('id',$request->subcategory_id)->select('price')->first();
        $quantity =$request->quantity;
        $total=$quantity*$price['price'];
        $reservationResult =   \App\Request::create(array_merge($request->all(),[
                'subcategory_id'=>$request->subcategory_id,
                'user_id'=>$user,
                'quantity' =>$quantity,
                'total'=>$total
            ]));
        //dd($reservationResult);
        $reservationResult->save();
            $response=[
                'message'=>'Request request sent successfully',
                'status'=>'200',
            ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = \App\User::where('jwt_token', $jwt)->first();
        $request = \App\Request::where('user_id', $user->id)->get();
        //$requestId=\App\Request::find($request->id);

        $res_item = [];
        $res_list  = [];
        foreach ($request as $res) {
            $res_item['id'] = $res->id;
            $res_item['user'] = $res->user_id;
            $res_item['subcategory_details'] = \App\Subcategory::where('id',$res->subcategory_id)->select('name_'.$lang.' as name','price')->first();
            $res_item['total'] = $res->total;
            $res_list[] = $res_item;
        }
        $response = [
            'message' => 'get data of User Requests successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->pluck('id')->first();
        $validator = Validator::make($request->all(), [
            'quantity'=>'required',
        ]);
        if ($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        Subcategory::where('id',$request->subcategory_id)->select('price')->first();
        $reservationResult=\App\Request::find($request->id);
        $reservationResult->quantity = $request->quantity;
        $reservationResult->save();
        $response=[
            'message'=>'Request request updated successfully',
            'status'=>'200',
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $reservation =\App\Request::find($request->id);
        if (\App\Request::destroy($request->id)){

            $response=[
                'message'=>'Order successfully deleted',
                'status'=>200,
            ];
            return \Response::json($response,200);
        }else{
            $response=[
                'message'=>'something went wrong',
                'status'=>401,
            ];
            return \Response::json($response,404);
        }
    }




}
