<?php

namespace App\Http\Controllers\Api\user;

use App\Category;
use App\TechnicalReport;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TechnicialReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $categories=Category::select('id','title_'.$lang. ' as title')
            ->where('status',1)->get();
        if (!empty($categories))
        {
            $response=[
                'message'=>'get data of categories successfully',
                'status'=>200,
                'data'=>$categories,
            ];
        }else{
            $response=[
                'message'=>'no data found',
                'status'=>404,
            ];
        }
        return \Response::json($response,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user = User::where('jwt_token',$jwt)->first();

        $validator = Validator::make($request->all(), [
            'category_id'=>'required',
            'components'=>'required',
            'note' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $technicalReport=new TechnicalReport();
        $technicalReport->user_id=$user->id;
        $technicalReport->category_id = $request->category_id;
        $technicalReport->components = $request->components;
        $technicalReport->note = $request->note;
        $technicalReport->save();
        $response=[
            'message'=>'technical report created successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
