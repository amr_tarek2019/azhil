<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicianSchedule extends Model
{
    protected $table='technicians_schedules';
    protected $fillable=['technician_id', 'order_id', 'day', 'time', 'date'];
}
