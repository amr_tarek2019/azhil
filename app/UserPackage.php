<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model
{
    protected $table='users_packages';
    protected $fillable=[
        'name', 'address', 'phone', 'email','price'];
}
