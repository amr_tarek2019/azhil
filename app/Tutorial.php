<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
    protected $table='tutorials';

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/tutorials/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/tutorials/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }
}
