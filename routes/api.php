<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'user','namespace'=>'Api\user'], function (){

    Route::group(['prefix'=>'tutorial'],function () {
        Route::get('', 'TutorialController@index');
    });


    Route::group(['prefix'=>'authentication'],function () {
        Route::post('register', 'AuthenticationController@register');
        Route::post('register/facebook','AuthenticationController@registerFacebook');
        Route::post('register/google', 'AuthenticationController@registerGoogle');
        Route::post('verification/code', 'AuthenticationController@verifyCode');
        Route::post('forget/password', 'AuthenticationController@forgetPassword');
        Route::post('reset/password', 'AuthenticationController@resetPassword');
        Route::post('login','AuthenticationController@login');
        Route::post('login/facebook','AuthenticationController@loginFacebook');
        Route::post('login/google','AuthenticationController@loginGoogle');
    });

    Route::group(['prefix'=>'categories'],function () {
        Route::get('', 'CategoriesController@index');
    });

    Route::group(['prefix'=>'subcategories'],function () {
        Route::post('', 'SubcategoriesController@index');
    });


    Route::group(['prefix'=>'cities'],function () {
        Route::get('', 'CitiesController@index');
    });
    Route::group(['prefix'=>'profile'],function () {
        Route::post('', 'ProfileController@index');
        Route::post('/update', 'ProfileController@update');
    });
    Route::group(['prefix'=>'about'],function () {
        Route::get('', 'AboutController@index');
    });

    Route::group(['prefix'=>'suggestions'],function () {
        Route::post('', 'SuggestionsController@store');
    });

    Route::group(['prefix'=>'requests'],function () {
        Route::post('', 'RequestsController@store');
        Route::post('/update', 'RequestsController@update');
        Route::post('/delete', 'RequestsController@destroy');
        Route::post('/show','RequestsController@show');
    });

    Route::group(['prefix'=>'order'],function () {
        Route::post('confirm', 'OrderController@create');
    });


    Route::group(['prefix'=>'notifications'],function () {
        Route::get('', 'NotificationsController@index');
    });

    Route::group(['prefix'=>'search'],function () {
        Route::get('', 'SearchController@index');
        Route::get('filter', 'SearchController@filterIndex');
        Route::get('category/filter', 'SearchController@getCategoryFilter');
    });

    Route::group(['prefix'=>'history'],function () {
        Route::post('', 'HistoryController@index');
    });

    Route::group(['prefix'=>'technical'],function () {
        Route::post('Report/get/categories', 'TechnicialReportController@index');
        Route::post('Report/store', 'TechnicialReportController@store');
    });

    Route::group(['prefix'=>'package'],function () {
        Route::post('get/data', 'UsersPackagesController@index');
        Route::post('store', 'UsersPackagesController@store');
        Route::get('show', 'UsersPackagesController@show');
        Route::post('update', 'UsersPackagesController@update');
    });
    Route::group(['prefix'=>'track'],function () {
        Route::get('technician','TrackTechnicianController@index');
    });

    Route::group(['prefix'=>'services'],function () {
        Route::get('','ServicesController@index');
    });

    Route::group(['prefix'=>'rate'],function () {
        Route::post('technician','RateTechnicianController@store');
    });

    Route::group(['prefix'=>'follow'],function () {
        Route::get('request','FollowRequestController@index');
    });

    Route::group(['prefix'=>'subscriptions'],function () {
        Route::get('statues/visits','SubscriptionStatuesController@indexVisits');
        Route::get('statues/inprogress','SubscriptionStatuesController@indexInProgress');
        Route::get('statues/indexDone','SubscriptionStatuesController@indexDone');
    });

});




Route::group(['prefix'=>'technician','namespace'=>'Api\technician'], function (){

    Route::group(['prefix'=>'tutorial'],function () {
        Route::get('', 'TutorialController@index');
    });

    Route::group(['prefix'=>'authentication'],function (){
        Route::post('login','AuthenticationController@login');
        Route::post('forget/password', 'AuthenticationController@forgetPassword');
        Route::post('verification/code', 'AuthenticationController@verifyCode');
        Route::post('reset/password', 'AuthenticationController@resetPassword');
    });

    Route::group(['prefix'=>'profile'],function (){
        Route::post('', 'ProfileController@index');
        Route::post('/update', 'ProfileController@update');
    });
    Route::group(['prefix'=>'about'],function () {
        Route::get('', 'AboutController@index');
    });

    Route::group(['prefix'=>'suggestions'],function () {
        Route::post('', 'SuggestionsController@store');
    });

    Route::group(['prefix'=>'rate'],function () {
        Route::post('order','RateOrderController@store');
    });

    Route::group(['prefix'=>'report'],function () {
        Route::get('order','ReportOrderController@index');
        Route::post('order/send','ReportOrderController@store');
    });

    Route::group(['prefix'=>'reply'],function () {
        Route::get('order','ReplyOrdersController@index');
        Route::post('order/store','ReplyOrdersController@store');
    });

    Route::group(['prefix'=>'requests'],function () {
        Route::get('get','ServicesInProgressController@index');
        Route::post('done','ServicesInProgressController@store');
    });

    Route::group(['prefix'=>'track'],function () {
        Route::get('user','TrackOrderController@index');
    });

    Route::group(['prefix'=>'Home'],function () {
        Route::get('available/orders','HomeController@indexAvailable');
        Route::get('schedule/orders','HomeController@indexSchedule');
    });
});