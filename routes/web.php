<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//
//Route::get('/backup',function(){
//    \Illuminate\Support\Facades\Artisan::call('backup:run');
//});
//
//Route::get('/login', function () {
//    return view('auth.login');
//});


Route::group(['prefix'=>'login','namespace'=>'Authentication'], function (){
    Route::group(['prefix'=>'user'],function () {
        Route::get('', 'AuthenticationController@index')->name('login');
        Route::post('auth', 'AuthenticationController@login')->name('user.login');

    });



});

Route::group(['prefix'=>'admin','namespace'=>'Dashboard'], function () {
    Route::group(['prefix' => 'dashboard'], function () {
        Route::get('/home', 'DashboardController@index')->name('dashboard');
        Route::post('', 'DashboardController@logout')->name('logout');
        Route::get('/backup','DashboardController@backup')->name('backup');

    });

    Route::group(['prefix' => 'profile'], function () {
        Route::get('', 'ProfileController@index')->name('profile');
        Route::post('/update/{id}','ProfileController@update')->name('profile.update');
    });

    Route::group(['prefix' => 'settings'], function () {
        Route::get('', 'SettingsController@index')->name('settings');
        Route::post('/update','SettingsController@update')->name('settings.update');
    });

    Route::prefix('suggestion')->group(function (){
        Route::get('','SuggestionsController@index')->name('suggestion.index');
        Route::get('/show/{id}','SuggestionsController@show')->name('suggestion.show');
        Route::post('/{id}','SuggestionsController@destroy')->name('suggestion.destroy');
    });

    Route::prefix('category')->group(function (){
        Route::get('','CategoryController@index')->name('category.index');
        Route::get('/create','CategoryController@create')->name('category.create');
        Route::post('/store','CategoryController@store')->name('category.store');
        Route::get('/edit/{id}','CategoryController@edit')->name('category.edit');
        Route::post('/update/{id}','CategoryController@update')->name('category.update');
        Route::post('/{id}','CategoryController@destroy')->name('category.destroy');
        Route::post('/status/{id}', 'CategoryController@updateStatus')->name('category.status');
    });

    Route::prefix('subcategory')->group(function (){
        Route::get('','SubcategoryController@index')->name('subcategory.index');
        Route::get('/create','SubcategoryController@create')->name('subcategory.create');
        Route::post('/store','SubcategoryController@store')->name('subcategory.store');
        Route::get('/edit/{id}','SubcategoryController@edit')->name('subcategory.edit');
        Route::post('/update/{id}','SubcategoryController@update')->name('subcategory.update');
        Route::post('/{id}','SubcategoryController@destroy')->name('subcategory.destroy');
        Route::post('/status/{id}', 'SubcategoryController@updateStatus')->name('subcategory.status');
    });

    Route::prefix('user')->group(function (){
        Route::get('','UsersController@index')->name('user.index');
        Route::get('/create','UsersController@create')->name('user.create');
        Route::post('/store','UsersController@store')->name('user.store');
        Route::get('/details/{id}','UsersController@show')->name('user.show');
        Route::get('/edit/{id}','UsersController@edit')->name('user.edit');
        Route::post('/update/{id}','UsersController@update')->name('user.update');
        Route::post('/{id}','UsersController@destroy')->name('user.destroy');
        Route::post('/status/{id}', 'UsersController@updateStatus')->name('user.status');
    });

    Route::prefix('admins')->group(function (){
        Route::get('','AdminsController@index')->name('admin.index');
        Route::get('/create','AdminsController@create')->name('admin.create');
        Route::post('/store','AdminsController@store')->name('admin.store');
        Route::get('/edit/{id}','AdminsController@edit')->name('admin.edit');
        Route::post('/update/{id}','AdminsController@update')->name('admin.update');
        Route::post('/{id}','AdminsController@destroy')->name('admin.destroy');
        Route::post('/status/{id}', 'AdminsController@updateStatus')->name('admin.status');
    });

    Route::prefix('member')->group(function (){
        Route::get('','MembersController@index')->name('member.index');
        Route::get('/create','MembersController@create')->name('member.create');
        Route::post('/store','MembersController@store')->name('member.store');
        Route::get('/edit/{id}','MembersController@edit')->name('member.edit');
        Route::post('/update/{id}','MembersController@update')->name('member.update');
        Route::post('/{id}','MembersController@destroy')->name('member.destroy');
        Route::post('/status/{id}', 'MembersController@updateStatus')->name('member.status');
    });

    Route::prefix('technician')->group(function (){
        Route::get('','TechniciansController@index')->name('technician.index');
        Route::get('/create','TechniciansController@create')->name('technician.create');
        Route::post('/store','TechniciansController@store')->name('technician.store');
        Route::get('/edit/{id}','TechniciansController@edit')->name('technician.edit');
        Route::post('/update/{id}','TechniciansController@update')->name('technician.update');
        Route::post('/{id}','TechniciansController@destroy')->name('technician.destroy');
        Route::post('/status/{id}', 'TechniciansController@updateStatus')->name('technician.status');
        Route::post('/status/availability/{id}', 'TechniciansController@technicianAvailability')->name('technician.availability');
    });

    Route::prefix('orders')->group(function (){
        Route::get('','OrdersController@index')->name('orders.index');
        Route::get('/edit/{id}','OrdersController@edit')->name('orders.edit');
        Route::post('/update/{id}','OrdersController@update')->name('orders.update');
        Route::post('/{id}','OrdersController@destroy')->name('orders.destroy');
        Route::get('/details/{id}','OrdersController@show')->name('orders.show');
    });

    Route::prefix('reservations')->group(function (){
        Route::get('','ReservationsController@index')->name('reservations.index');
        Route::get('/{id}','ReservationsController@show')->name('reservations.show');
        Route::post('/{id}','ReservationsController@destroy')->name('reservations.destroy');
        Route::get('/invoice/{id}','ReservationsController@generatePdf')->name('reservations.invoice.show');
    });

    Route::prefix('sliders')->group(function (){
        Route::get('','SlidersController@index')->name('sliders.index');
        Route::get('/create','SlidersController@create')->name('sliders.create');
        Route::post('/store','SlidersController@store')->name('sliders.store');
        Route::get('/edit/{id}','SlidersController@edit')->name('sliders.edit');
        Route::post('/update/{id}','SlidersController@update')->name('sliders.update');
        Route::post('/{id}','SlidersController@destroy')->name('sliders.destroy');
        Route::post('/status/{id}', 'SlidersController@updateStatus')->name('sliders.status');
    });
});
